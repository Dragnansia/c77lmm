/* preferences.rs
 *
 * Copyright 2024 dragnansia
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

use adw::prelude::*;
use adw::subclass::prelude::*;
use gtk::prelude::*;
use gtk::{gio, glib};

use crate::window::C77lmmWindow;

mod imp {
    use super::*;

    #[derive(Debug, Default, gtk::CompositeTemplate)]
    #[template(resource = "/org/dragnansia/c77lmm/preferences/preferences.ui")]
    pub struct C77lmmPreferences {}

    #[glib::object_subclass]
    impl ObjectSubclass for C77lmmPreferences {
        const NAME: &'static str = "C77lmmPreferencesDialog";
        type Type = super::C77lmmPreferences;
        type ParentType = adw::PreferencesDialog;

        fn class_init(klass: &mut Self::Class) {
            klass.bind_template();
        }

        fn instance_init(obj: &glib::subclass::InitializingObject<Self>) {
            obj.init_template();
        }
    }

    impl ObjectImpl for C77lmmPreferences {
        fn constructed(&self) {
            self.parent_constructed();
        }
    }

    impl WidgetImpl for C77lmmPreferences {}
    impl WindowImpl for C77lmmPreferences {}
    impl AdwDialogImpl for C77lmmPreferences {}
    impl PreferencesDialogImpl for C77lmmPreferences {}
}

glib::wrapper! {
    pub struct C77lmmPreferences(ObjectSubclass<imp::C77lmmPreferences>)
        @extends gtk::Widget, gtk::Window, adw::Dialog, adw::PreferencesDialog,
        @implements gio::ActionGroup, gio::ActionMap;
}

impl C77lmmPreferences {
    pub fn new(window: &C77lmmWindow) -> Self {
        let win = glib::Object::builder::<Self>().build();

        win.setup_pages(window);
        win
    }

    fn setup_pages(&self, window: &C77lmmWindow) {
        self.add(&self.setup_settings_page(window));
        self.set_visible_page_name("settings");
    }

    fn setup_settings_page(&self, window: &C77lmmWindow) -> adw::PreferencesPage {
        let load_profile_row = adw::SwitchRow::builder()
            .title("Load Profile")
            .subtitle("If we load the last profile")
            .build();

        window
            .settings()
            .bind("load-last-profile", &load_profile_row, "active")
            .build();

        let profile_group = adw::PreferencesGroup::builder()
            .name("profile")
            .title("Profile")
            .build();

        profile_group.add(&load_profile_row);
        let settings = adw::PreferencesPage::builder()
            .name("settings")
            .title("Settings")
            .icon_name("emblem-system-symbolic")
            .build();

        settings.add(&profile_group);
        settings
    }
}

