/* window.rs
 *
 * Copyright 2024 dragnansia
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

use std::cell::{OnceCell, RefCell};
use std::fs::File;

use adw::prelude::*;
use adw::subclass::prelude::*;
use gtk::prelude::*;
use gtk::{gio, glib};

use crate::{
    config::APPLICATION_ID,
    preferences::C77lmmPreferences,
    profiles::{ProfileData, ProfileObject},
    utils::get_profiles_path,
};

mod imp {
    use super::*;

    #[derive(Debug, Default, gtk::CompositeTemplate)]
    #[template(resource = "/org/dragnansia/c77lmm/window/window.ui")]
    pub struct C77lmmWindow {
        // Template widgets
        #[template_child]
        pub header_bar: TemplateChild<adw::HeaderBar>,
        #[template_child]
        pub stack: TemplateChild<gtk::Stack>,

        pub settings: OnceCell<gio::Settings>,
        pub profiles: RefCell<Option<gio::ListStore>>,
    }

    #[glib::object_subclass]
    impl ObjectSubclass for C77lmmWindow {
        const NAME: &'static str = "C77lmmWindow";
        type Type = super::C77lmmWindow;
        type ParentType = adw::ApplicationWindow;

        fn class_init(klass: &mut Self::Class) {
            klass.bind_template();
        }

        fn instance_init(obj: &glib::subclass::InitializingObject<Self>) {
            obj.init_template();
        }
    }

    impl ObjectImpl for C77lmmWindow {
        fn constructed(&self) {
            self.parent_constructed();

            println!("{APPLICATION_ID}");
            if APPLICATION_ID.ends_with("Devel") {
                self.obj().add_css_class("devel");
            }

            let obj = self.obj();
            obj.setup();
            obj.load_profile();
        }
    }

    impl WidgetImpl for C77lmmWindow {}
    impl WindowImpl for C77lmmWindow {
        fn close_request(&self) -> glib::Propagation {
            let datas: Vec<ProfileData> = self
                .obj()
                .profiles()
                .iter::<ProfileObject>()
                .filter_map(|p| p.ok())
                .map(|p| p.to_data())
                .collect();

            let file = File::create(get_profiles_path()).expect("Could not create json file.");
            serde_json::to_writer(file, &datas).expect("Could not write data to json file");

            self.parent_close_request()
        }
    }
    impl ApplicationWindowImpl for C77lmmWindow {}
    impl AdwApplicationWindowImpl for C77lmmWindow {}
}

glib::wrapper! {
    pub struct C77lmmWindow(ObjectSubclass<imp::C77lmmWindow>)
        @extends gtk::Widget, gtk::Window, gtk::ApplicationWindow, adw::ApplicationWindow,
        @implements gio::ActionGroup, gio::ActionMap;
}

impl C77lmmWindow {
    pub fn new<P: IsA<gtk::Application>>(application: &P) -> Self {
        glib::Object::builder()
            .property("application", application)
            .build()
    }

    pub fn settings(&self) -> &gio::Settings {
        self.imp()
            .settings
            .get()
            .expect("`settings` should be set in `setup_settings`.")
    }

    fn profiles(&self) -> gio::ListStore {
        self.imp()
            .profiles
            .borrow()
            .clone()
            .expect("Could not get current profiles.")
    }

    fn setup(&self) {
        let settings = gio::Settings::new(APPLICATION_ID);
        self.imp()
            .settings
            .set(settings)
            .expect("`settings` should not be set before calling `setup_settings`.");

        // Setup profile list
        let profile_model = gio::ListStore::new::<ProfileObject>();
        self.imp().profiles.replace(Some(profile_model));

        // Set last selected profiles
        let load_last_profile = self.settings().boolean("load-last-profile");
        if load_last_profile {
            let profile_name = self.settings().string("last-select-profile");
            if !profile_name.is_empty() {
                self.set_profile(&profile_name);
            }
        }

        // Setup Actions
        let new_profile_action = gio::ActionEntry::builder("add-new-profile")
            .activate(move |win: &Self, _, _| win.add_new_profile())
            .build();
        let open_preferences_windows = gio::ActionEntry::builder("open-preferences")
            .activate(move |win: &Self, _, _| win.open_preferences_window())
            .build();
        self.add_action_entries([new_profile_action, open_preferences_windows]);
    }

    fn open_preferences_window(&self) {
        let preferences = C77lmmPreferences::new(self);
        // Multiple applicable items for present fonction
        adw::prelude::AdwDialogExt::present(&preferences, self);
    }

    fn load_profile(&self) {
        let path = crate::utils::get_profiles_path();
        let objects = if let Ok(file) = File::open(&path) {
            let datas: Vec<ProfileData> = serde_json::from_reader(file).unwrap_or_default();

            datas.into_iter().map(ProfileObject::from_data).collect()
        } else {
            vec![]
        };

        self.profiles().extend_from_slice(&objects);
        self.update_stack_view();
    }

    fn set_profile(&self, name: &str) {
        let _ = self.profiles();
        self.settings()
            .set_string("last-select-profile", name)
            .expect("Can't set last selected profile name");
    }

    fn add_new_profile(&self) {
        self.profiles()
            .append(&ProfileObject::from_data(ProfileData { name: "Profile Test".into() }));

        self.update_stack_view();
    }

    fn update_stack_view(&self) {
        let stack_name = if self.profiles().n_items() > 0 {
            "menu"
        } else {
            "placeholder"
        };
        self.imp().stack.set_visible_child_name(stack_name);
    }
}

